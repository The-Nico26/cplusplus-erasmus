#include "pch.h"
#include <iostream>
#include <stdio.h>

using namespace std;

long sizeFile(char *path) {
	cout << "Path: " << path << endl;
	long size = 0;
	FILE *pFile;

	pFile = fopen(path, "rb");

	if (pFile == NULL) {
		perror("Error fopen");
	}
	else {
		fseek(pFile, 0, SEEK_END);
		size = ftell(pFile);
		fclose(pFile);
	}

	return size;
}

int main() {
	char path[] = "data/messages_2_G1.grib";
	Decoder d = Decoder();
	d.FindSize(path);

	char searchStr[] = "GRIB";
	long pos = d.FindStrPos(searchStr);

	char searchStr2[] = "7777";
	long pos2 = d.FindStrPos(searchStr2);

	cout << "Position GRIB: " << pos << endl;
	cout << "Position 7777: " << pos2 << endl;

	char ci;
	cin >> ci;
	return 1;
}