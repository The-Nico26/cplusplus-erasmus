#include "pch.h"
#include <iostream>
using namespace std;

class Book {
public:
	int mass;
public:
	Book() :mass(1) {};
	Book(int mas) : mass(mas) {};
};
class Bag {
private:
	int max, actuel;
public:
	Bag() : max(10), actuel(0) {};
	Bag(int ma) : max(ma), actuel(0) {};
	void operator+(Book *B);
};
void Bag::operator+(Book *B) {
	if (B->mass + actuel > max) throw "Max";
	actuel += B->mass;
}

/*int main() {
	Book Bo1 = Book(5), Bo2 = Book();
	Bag Ba = Bag();
	Ba + &Bo1;

	return 1;
}*/