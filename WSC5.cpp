#include "pch.h"
#include <stdarg.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

float calculate(char *flag, ...) {
	va_list args;
	va_start(args, flag);
	//int number = va_arg(args, int);
	float number = 0;
	while (*flag) {
		switch (*(flag++)) {
			case '+':
				number += va_arg(args, float);
				break;
			case '-':
				number -= va_arg(args, float);
				break;
			case '/':
				number /= va_arg(args, float);
				break;
			case '*':
				number *= va_arg(args, float);
		}
	}
	va_end(args);
	return number;
}
/*
auto getType(char *n, char *v) {
	if (n == "i")
		return atoi(v);
	else if (n == "d")
		return atof(v);
	else
		return atod(v);
}
*/
/*
int main() {
	char str[] = "+-";

	cout << calculate(str, 5.0f, 1.5f) << endl;
	return 0;
}*/