#include "pch.h"
#include <iostream>
using namespace std;

template <typename T>
T* sort(T table[], int number) {
	for (int i = number; i > 0; i--)
	{
		bool sortOK = true;
		for (int j = 0; j < i - 1; j++) {
			if (table[j + 1] < table[j]) {
				T var = table[j];
				table[j] = table[j + 1];
				table[j + 1] = var;
				sortOK = false;
			}
		}
		if (sortOK) return table;
	}
	return table;
};
/*
int main() {
	char t[] = { 'b', 'c', 'z', 'r', 'a', 'm' };
	//int t[] = { 9, 5, 2, 11, 4, 0 };

	int nu = 6;
	for (int i = 0; i < nu; i++)
	{
		cout << t[i] << ',';
	}
	cout << endl;
	char *a = sort<char>(t, nu);
	
	for (int i = 0; i < nu; i++)
	{
		cout << a[i] << ',';
	}
}*/