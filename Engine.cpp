#include "pch.h"
#include "Engine.h"
#include <iostream>
using namespace std;

Engine::Engine(int nn) : n(nn) {};
void Engine::Info() {
	cout << "Engine Class\n";
}


E1::E1(int m) : Engine(m) {};
void E1::Info() {
	cout << "E1 Class\n";
}


E2::E2(int m) : Engine(m) {};
void E2::Info() {
	cout << "E2 Class\n";
}

H::H(int o, int p) : E1(o), E2(p), Engine(o) {};
void H::Info() {
	cout << "H Class\n";
}
