#include "pch.h"
#include <iostream>

using namespace std;

class Person {
public:
	Person() {};
};
class Bus {
private:
	int am, max;
public:
	Bus();
	int N() { return max; };
	Bus& operator+(Person &P1);
};
Bus::Bus(): am(0), max(40){}
Bus& Bus::operator+(Person &P1) {
	if (am >= max) throw "Max";
	am++;
	return *this;
}

/*int main() {
	Person P1;
	Bus B;
	cout << B.N();
	try {
		B = B + P1;
	}
	catch (const char* s) {
		cout << s << endl;
	}
	return 1;
}*/