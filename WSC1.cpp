#include "pch.h"
#include <cmath>
#include <iostream>

double Lineare(double);
double Sq(double);
double IntTrap(double (*Fun)(double), double, double);

/*int main()
{
	double a, b, s;
	std::cin >> a;
	std::cin >> b;

	s = IntTrap(Lineare, a, b);
	std::cout << "\nNumber Lineare: " << s << "\n\n" << std::endl;
	s = IntTrap(Sq, a, b);
	std::cout << "\nNumber Sq: " << s << "\n\n" << std::endl;
	return 1;
}*/

double IntTrap(double (*Fun)(double), double a, double b) {
	double i = 0.0, io;
	int n = 2;
	do {
		n = n * 2;
		io = i;
		double h = (b - a) / n;
		double sum = 0.0;
		for (int x = 1; x < n; x++)
		{
			double xi = a + x * h;
			sum += Fun(xi);
		}
		i = h * ((Fun(a) + Fun(b)) / 2.0 + sum);

	} while (abs(i - io) > 1e-3);

	return i;
}

double Lineare(double x) {
	return x;
}
double Sq(double x) {
	return x * x + x - 1;
}