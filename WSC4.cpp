#include "pch.h"
#include <iostream>

using namespace std;
class Comp {
public:
	int max, min;
public:
	Comp();
	Comp(int ma, int mi);
	Comp operator+(Comp C1);
friend
	ostream& operator<<(ostream &os, Comp C1);
friend
	istream& operator>>(istream &in, Comp *C1);
};

Comp::Comp() : max(0), min(0) {};
Comp::Comp(int ma, int mi) : max(ma), min(mi) {};
Comp Comp::operator+(Comp C1) {
	return Comp(max + C1.max, min + C1.min);
}
ostream& operator<<(ostream &os, Comp C1) {
	os << C1.max << C1.min << endl;
	return os;
}
istream& operator>>(istream &in, Comp *C1) {
	in >> C1->max;
	in >> C1->min;
	return in;
}

/*int main() {
	Comp z1 = Comp(1, 2), z2 = Comp(3, 4), z3, z4 = Comp();
	z3 = z1 + z2;
	cin >> &z4;

	cout << z4;
	return 0;
}*/