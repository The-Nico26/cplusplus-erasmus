#include "pch.h"
#include <cmath>
#include <iostream>
#include <fstream>

using namespace std;
/*
template <typename T>
T moyenne(T value[], int number) {
	T tt = 0;
	for(int a = 0; a < number; a++)
	{
		tt += value[a];
	}
	return tt/number;
}
*/

template <class C> class Array
{
protected:
	C *T;
	int n;
public:
	Array(int nm);
	~Array();
	C& operator[](int i);
	int N() { return n; }
};

template <class C> Array<C>::Array(int nm) {
	if (nm < 1)throw "Exception";
	T = new C[nm];
	n = nm;
	for (int index = 0; index < nm; index++) {
		cin >> T[index];
	}

}

template <class C> Array<C>::~Array() {
	if(T) delete(T);
}

template <class C>C& Array<C>::operator[](int i) {
	if (i < 0 || i >= n) throw "Out array";
	return T[i];
}
/*
int main() {
	double result;

	/* @TODO Tome 1
	int number[] = { 5,6,7,1 };
	result = moyenne<int>(number, 4);
	cout << result << endl;
	

	/* @TODO Tome 2
	char fname[256];
	cin >> fname;
	ifstream Read(fname);
	int n;
	Read >> n;
	double *v = new double[n];
	for (int index = 0; index < n; index++) {
		Read >> v[index];
	}
	Read.close();
	result = moyenne<double>(v, n);
	cout << "Result: " << result << endl;
	
	/* @TODO Tome 3
	Array<char> A(5);
	for (int index = 0; index < A.N(); index++) {
		cout << A[index] << " ";
	}
	return 0;
}*/