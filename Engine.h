#ifndef ENGINE_H
#define ENGINE_H

class Engine {
private:
	int n;
public:
	Engine(int nn);
	~Engine() {};
	virtual void Info();
};

class E1 : virtual public Engine {
public:
	E1(int m);
	~E1() {};
	void Info();
};

class E2 : virtual public Engine {
public:
	E2(int m);
	~E2() {};
	void Info();
};

class H : public E1, public E2 {
public:
	H(int o, int p);
	~H() {};
	void Info();
};
#endif // !ENGINE_H
